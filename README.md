# BITS Framework

BITS Framework is simple and powerfull php framework developed by BITS Team and supported by [BITS Cloud Hosting](https://billing.bits.co.id) - BITS Cloud Hosting.

## Install

Install BITS Framework

```
cd /var/www
git clone git@gitlab.com:project-bits/BITS-Framework.git bits-framework
cd bits-framework
composer update
bower install bits-frontend
npm install
grunt
```

## Configurations

### Nginx + PHP-FPM

Install Nginx & PHP-FPM (Debian & Ubuntu)

```
apt-get install nginx php5-fpm
```
Nginx Configuration for BITS Framework

```
cd /etc/nginx/sites-available
nano framework.dev
```

Insert the following Nginx Config
```
server {
    listen 80;

    root /var/www/bits-framework;
    index index.php index.html index.htm;
    server_name framework.dev;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```
Enable Nginx Configuration
```
ln -s /etc/nginx/sites-available/framework.dev /etc/nginx/sites-enabled/framework.dev
```
Restart Nginx & PHP-FPM
```
service php5-fpm restart
service nginx restart
```
### Database Configuration

Install MariaDB
```
apt-get install mariadb-server php5-mysql
```

Install MySQL
```
apt-get install mysql-server php5-mysql
```

Install phpMyAdmin
```
apt-get install phpmyadmin
ln -s /usr/share/phpmyadmin /var/www/framework/phpmyadmin
```
#### Import Database
Open http://framework.dev/phpmyadmin in your browser. Create database and import framework.sql to your database.

#### Start BITS Framework
Open http://framework.dev in your browser & enjoy it !

### Created & Supported by :

 * [BITS.CO.ID](https://bits.co.id) - Banten IT Solutions official sites
 * [BITS.MY.ID](http://bits.my.id) - BITS Framework
 * [Banten IT Solutions](http://www.banten-it.com) - Banten IT Solutions official sites
 * [Nurul Imam Studio](http://www.nurulimam.com) - Nurul Imam at Project Manager
