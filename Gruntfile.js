module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            css: {
                src: [
                    'assets/bootstrap/dist/css/bootstrap.min.css',
                    'assets/font-awesome/css/font-awesome.min.css',
                    'assets/animate.css/animate.min.css',
                    'assets/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
                    'assets/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                    'assets/bootstrap-fileinput/css/fileinput.min.css',
                    'assets/jquery.gritter/css/jquery.gritter.css',
                    'assets/metisMenu/dist/metisMenu.min.css',
                    'assets/select2/dist/css/select2.min.css',
                    'assets/summernote/dist/summernote.css',
                    'assets/toastr/toastr.min.css',
                    'assets/bootstrap-table/dist/bootstrap-table.min.css',
                    'assets/codemirror/lib/codemirror.css',
                    'assets/codemirror/theme/seti.css',
                    'assets/bits-frontend/css/bits.css',
                    'assets/bits-frontend/css/custom.css'
                ],
                dest: 'assets/bits-frontend/css/style.css'
            },
            js: {
                src: [
                    'assets/jquery/dist/jquery.min.js',
                    'assets/bootstrap/dist/js/bootstrap.min.js',
                    'assets/bootstrap-switch/dist/js/bootstrap-switch.min.js',
                    'assets/bootstrap-validator/dist/validator.min.js',
                    'assets/bootstrap-fileinput/js/fileinput.min.js',
                    'assets/jquery.gritter/js/jquery.gritter.min.js',
                    'assets/metisMenu/dist/metisMenu.min.js',
                    'assets/moment/min/moment.min.js',
                    'assets/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                    'assets/pace/pace.min.js',
                    'assets/select2/dist/js/select2.min.js',
                    'assets/toastr/toastr.min.js',
                    'assets/bootstrap-table/dist/bootstrap-table.min.js',
                    'assets/codemirror/lib/codemirror.js',
                    'assets/codemirror/mode/xml/xml.js',
                    'assets/summernote/dist/summernote.min.js',
                    'assets/jquery-backstretch/jquery.backstretch.min.js',
                    'assets/bits-frontend/js/bits.js',
                    'assets/menu/menu.js'
                ],
                dest: 'assets/bits-frontend/js/script.js'
            }
        },
        cssmin: {
            css: {
                src: 'assets/bits-frontend/css/style.css',
                dest: 'assets/bits-frontend/css/style.min.css'
            }
        },
        uglify: {
            js: {
                files: {
                    'assets/bits-frontend/js/script.js': ['assets/bits-frontend/js/script.js']
                }
            }
        },
        watch: {
            files: [
                    'assets/bootstrap/dist/css/bootstrap.min.css',
                    'assets/font-awesome/css/font-awesome.min.css',
                    'assets/animate.css/animate.min.css',
                    'assets/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
                    'assets/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                    'assets/bootstrap-fileinput/css/fileinput.min.css',
                    'assets/jquery.gritter/css/jquery.gritter.css',
                    'assets/metisMenu/dist/metisMenu.min.css',
                    'assets/select2/dist/css/select2.min.css',
                    'assets/summernote/dist/summernote.css',
                    'assets/toastr/toastr.min.css',
                    'assets/bootstrap-table/dist/bootstrap-table.min.css',
                    'assets/codemirror/lib/codemirror.css',
                    'assets/codemirror/theme/seti.css',
                    'assets/bits-frontend/css/bits.css',
                    'assets/bits-frontend/css/custom.css',
                    'assets/jquery/dist/jquery.min.js',
                    'assets/bootstrap/dist/js/bootstrap.min.js',
                    'assets/bootstrap-switch/dist/js/bootstrap-switch.min.js',
                    'assets/bootstrap-validator/dist/validator.min.js',
                    'assets/bootstrap-fileinput/js/fileinput.min.js',
                    'assets/jquery.gritter/js/jquery.gritter.min.js',
                    'assets/metisMenu/dist/metisMenu.min.js',
                    'assets/moment/min/moment.min.js',
                    'assets/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                    'assets/pace/pace.min.js',
                    'assets/select2/dist/js/select2.min.js',
                    'assets/summernote/dist/summernote.min.js',
                    'assets/toastr/toastr.min.js',
                    'assets/bootstrap-table/dist/bootstrap-table.min.js',
                    'assets/codemirror/lib/codemirror.js',
                    'assets/codemirror/mode/xml/xml.js',
                    'assets/jquery-backstretch/jquery.backstretch.min.js',
                    'assets/bits-frontend/js/bits.js',
                    'assets/menu/menu.js'
                ],
            tasks: ['concat', 'cssmin', 'uglify'],
            options: {
              livereload: true,
            }
        },
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', ['concat:css', 'cssmin:css', 'concat:js', 'uglify:js', 'watch']);
};
