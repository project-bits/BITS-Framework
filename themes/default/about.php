<div class="wrapper">
    <div class="movie">BITS Framework</div>
    <div class="job">created by</div>
    <div class="name">BITS Team</div>
    <div class="job">support by</div>
    <div class="name">BITS Cloud Hosting</div>
    <div class="job">Project Leaders</div>
    <div class="name">Nurul Imam</div>
    <div class="job">Core Developer</div>
    <div class="name">Mulyadi Sopiyani</div>
    <div class="job">Module Developer</div>
    <div class="name">Ahkmad Hady</div>
    <div class="job">UI / UX Designer</div>
    <div class="name">Zaenal Muttaqien</div>
    <div class="job">Documentation and Support</div>
    <div class="name">Rokib</div>
    <div class="job">External Libraries</div>
    <div class="name">Klein Router</div>
    <div class="job">Frontend Libraries</div>
    <div class="name">jQuery <br>
                    Bootstrap <br>
                    Bootstrap Switch <br>
                    Bootstrap Date Time Picker <br>
                    Bootstrap File Input <br>
                    Bootstrap Validator <br>
                    Bootstrap Table <br>
                    Font Awesome <br>
                    Animate.css <br>
                    jQuery Gritter <br>
                    Metis Menu <br>
                    Select2 <br>
                    Summernote <br>
                    Toastr <br>
                    Codemirror <br>
                    Moment.js <br>
                    Pace <br>
                    jQuery Backstretch
    </div>
    <div class="job">Contributing Developers</div>
    <div class="name">Put your code here...</div>
</div>
