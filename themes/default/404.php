<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>404 Not Found - That page doesn't exist !</title>
    <link href="/themes/default/css/style.css" rel="stylesheet">
</head>
<body>
    <div class="container welcome">
        <h1 class="text-center"><span class="change">404 Not Found - That page doesn't exist !</span></h1>
    </div>
    <script type="text/javascript" src="/themes/default/js/jquery.js"></script>
    <script type="text/javascript" src="/themes/default/js/script.js"></script>
</body>
</html>
