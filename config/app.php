<?php
/**
 * Database Configuration of BITS Frameworks
 */
define('DBHOST', 'localhost');
define('DBUSER', 'root');
define('DBPASS', 'suck-IT26');
define('DBNAME', 'framework');

/**
 * Basic Web Apps Configurations
 */
define('APPNAME', 'BITS Framework');
define('APPDESC', 'Simple PHP Framework');
define('APPMAIL', 'info@bits.co.id');
define('APPURL', 'http://framework.dev');
define('THEME_PATH', $_SERVER["DOCUMENT_ROOT"].'/themes/default/');

require_once 'app/Models/lib.php';
