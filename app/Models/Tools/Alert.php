<?php namespace Tools;

/**
 * BITS Alert Services.
 *
 * Class to simply use Alert Sevices support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @author Nurul Imam
 *
 * @link https://bits.co.id Banten IT Solutions
 *
 * @version 1.1
 */
class Alert
{
    public static function show()
    {
        if (isset($_SESSION['message'])) {
            echo '
            <div class="alert alert-'.$_SESSION['alert'].' fade in">
                '.$_SESSION['message'].'
            </div>';
            self::delete();
        }
    }

    public static function view()
    {
        if (isset($_SESSION['message'])) {
            echo "
                $(document).ready(function() {
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            positionClass: 'toast-top-right',
                            showDuration: 400,
                            hideDuration: 1000,
                            extendedTimeOut: 1000,
                            showEasing: 'swing',
                            hideEasing: 'linear',
                            showMethod: 'fadeIn',
                            hideMethod: 'fadeOut',
                            timeOut: 7000
                        };
                        toastr.".$_SESSION['alert']."("."'".$_SESSION['message']."');
                    }, 1300);
                });";
        }
        self::delete();
    }

    public static function add($type, $message)
    {
        $_SESSION['alert']   = $type;
        $_SESSION['message'] = $message;
    }

    public static function delete()
    {
        unset($_SESSION['alert']);
        unset($_SESSION['message']);
    }
}
