<?php namespace Tools;

/**
* Convert Date Format
*
* Allow to generate format from date().
*/
class Date
{
    /**
     * Convert Date Format.
     * @param  date $date Date generated from date().
     * @param  string $type stripe, slash, time
     * @return date  Date formatted.
     */
    public static function convert($date, $type = "stripe")
    {
        if ($type == "stripe") {
            $data = date('d - F - Y', strtotime($date));
        } elseif ($type == "slash") {
            $data = date('d/m/Y', strtotime($date));
        } elseif ($type == "time") {
            $data = date('H:i', strtotime($date));
        } elseif ($type == "timestamp") {
            $data = date('Y-F-d H:i:s', strtotime($date));
        } elseif ($type == "usa") {
            $data = date('F d, Y', strtotime($date));
        } elseif ($type == "day") {
            $data = date('d', strtotime($date));
        } elseif ($type == "month") {
            $data = date('M', strtotime($date));
        } elseif ($type == "year") {
            $data = date('Y', strtotime($date));
        } else {
            $data = date('Y-m-d');
        }
        return $data;
    }
}
