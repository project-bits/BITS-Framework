<?php

namespace App;

use BITS\BITS;
use BITS\Upload;
use Tools\Alert;

/**
 * User CRUD Services.
 *
 * Class to simply use CRUD Data Management support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @author Nurul Imam
 *
 * @link https://bits.co.id Banten IT Solutions
 *
 * @version 1.1
 */
class User
{
    /**
     * Get All Users data.
     * @return array Data all users.
     */
    public static function all()
    {
        return BITS::all("users");
    }

    /**
     * Add new user.
     */
    public static function add()
    {
        if ($_FILES['photo']['name'] != '') {
            $_POST['photo'] = Upload::photo();
        } else {
            $_POST['photo'] = '';
        }
        $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);

        BITS::add('users', ['username', 'name', 'email', 'telp', 'photo', 'address', 'password', 'level']);
        Alert::add("success", "Successfully created !");
    }

    /**
     * Find User by ID
     * @param  int $ids ID of user.
     * @return array    Data user.
     */
    public static function find($ids)
    {
        return BITS::find("users", "id", $ids);
    }

    /**
     * Update User by ID
     * @return action Redirect to List all users.
     */
    public static function update()
    {
        /**
         * Change User Password if fill the password field.
         */
        if (!empty($_POST['password'])) {
            $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
            BITS::update("users", ['password'], "id", $_POST['id']);
            Alert::add("success", "Password updated !");
        }

        /**
         * Change Photo if upload the photo field.
         */
        if ($_FILES['photo']['name'] != '') {
            $_POST['photo'] = Upload::photo();
            BITS::update("users", ['photo'], "id", $_POST['id']);
            Alert::add("success", "Photo updated !");
        }

        /**
         * Update User Data Without Photo & Password.
         */
        BITS::update('users', ['username', 'name', 'email', 'telp', 'address'], "id", $_POST['id']);
        Alert::add("success", "Successfully updated !");
    }

    /**
     * Delete User by ID.
     * @param  int $ids ID user.
     * @return action   Redirect to List all users.
     */
    public static function delete($ids)
    {
        Alert::add("success", "Successfully removed !");
        return BITS::delete("users", "id", $ids);
    }

    public static function getName()
    {
        $data = BITS::find("users", "username", isset($_SESSION['username']) ? $_SESSION['username'] : "");
        echo isset($data[0]['name']) ? $data[0]['name'] : "";
    }

    public static function getPhoto($username = "")
    {
        $data = BITS::find("users", "username", ($username != "") ? $username : $_SESSION['username']);
        echo ($data[0]['photo'] != "") ? $data[0]['photo'] : "/assets/bits-frontend/files/img/default.jpg";
    }

    public static function getLevel()
    {
        $data = BITS::find("users", "username", isset($_SESSION['username']) ? $_SESSION['username'] : "");
        echo isset($data[0]['level']) ? $data[0]['level'] : "";
    }

    public static function getMail()
    {
        $data = BITS::find("users", "username", isset($_SESSION['username']) ? $_SESSION['username'] : "");
        echo isset($data[0]['email']) ? $data[0]['email'] : "";
    }
}
