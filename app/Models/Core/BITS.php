<?php namespace BITS;

/**
 * BITS CRUD Services.
 *
 * Class to simply use CRUD Data Management support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @author Nurul Imam
 *
 * @link https://bits.co.id Banten IT Solutions
 *
 * @version 1.1
 */
class BITS extends Query
{
    public static $appname = APPNAME;
    public static $appdesc = APPDESC;
    public static $appmail = APPMAIL;

    public static function appname($name = null)
    {
        if (!is_null($name)) {
            return $name;
        } else {
            return self::$appname;
        }
    }

    public static function appdesc($slog = null)
    {
        if (!is_null($slog)) {
            return $slog;
        } else {
            return self::$appdesc;
        }
    }

    public static function appmail($mail = null)
    {
        if (!is_null($mail)) {
            return $mail;
        } else {
            return self::$appmail;
        }
    }
}
