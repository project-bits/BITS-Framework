<?php namespace BITS;

/**
 * BITS Level Services.
 *
 * Class to simply use Level Sevices support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @author Nurul Imam
 *
 * @link https://bits.co.id Banten IT Solutions
 *
 * @version 1.1
 */
class Level extends Auth
{
    public static function isAdmin()
    {
        if (isset($_SESSION['level'])) {
            if ($_SESSION['level'] == "admin") {
                return "ok";
            }
        }
    }

    public static function isUser()
    {
        if (isset($_SESSION['level'])) {
            if ($_SESSION['level'] == "user") {
                return "ok";
            }
        }
    }
}
