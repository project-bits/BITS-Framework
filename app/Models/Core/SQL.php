<?php namespace BITS;

/**
 * BITS CRUD Services.
 *
 * Class to simply use CRUD Data Management support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @author Nurul Imam
 *
 * @link https://bits.co.id Banten IT Solutions
 *
 * @version 1.1
 */
class SQL extends DB
{
    /**
     * PDO Object Connections.
     *
     * @var object
     */
    protected static $data;

    /**
     * Querying SQL Syntax.
     *
     * @var string
     */
    protected static $query;

    /**
     * Convert Array to String used to Insert Query.
     *
     * @param array $isi Array object to passed parameter bind and value.
     *
     * @return string Data string to query value.
     */
    public static function loopAdd($isi)
    {
        // Initialize empty string
        $isinya = '';

        /*
         * Loop array and add string value to PDO style.
         * Data can used to bind and value PDO Query.
         */
        foreach ($isi[0] as $hasil) {
            $isinya .= ':'.$hasil.', ';
        }

        return $isinya;
    }

    /**
     * Convert Array to String used to Update Query.
     *
     * @param array $isi Array object to passed parameter bind and value.
     *
     * @return string Data string to query value.
     */
    public static function loopUpdate($isi)
    {
        // Initialize empty string
        $isinya = '';

        /*
         * Loop array and add string value to PDO style.
         * Data can used to bind and value PDO Query.
         */
        foreach ($isi[0] as $hasil) {
            $isinya .= $hasil.' = :'.$hasil.', ';
        }

        return $isinya;
    }

    /**
     * PDO Bind Param dynamic value.
     *
     * @param array $isi Array object to passed parameter bind and value from $_POST.
     *
     * @return object Bind PDO Syntax to bind parameter.
     */
    public static function bind($isi)
    {
        for ($i = 0; $i < count($isi); ++$i) {
            self::$data->bindParam(':'.$isi[$i], $_POST[$isi[$i]]);
        }
    }

    /**
     * PDO Bind Param custom value.
     *
     * @param array $isi Array object to passed custom bind and value parameter.
     *
     * @return object Bind PDO Syntax to bind parameter.
     */
    public static function bindCustom($isi)
    {
        for ($i = 0; $i < count($isi[0]); ++$i) {
            self::$data->bindParam($isi[0][$i], $isi[1][$i]);
        }
    }

    /**
     * PDO Prepare SQL Query.
     *
     * @return object PDO Prepare Syntax.
     */
    public static function prepare()
    {
        self::$data = parent::$connection->prepare(self::$query);
    }

    /**
     * PDO Execute SQL Query.
     *
     * @return object Execute SQL Query.
     */
    public static function execute()
    {
        self::$data->execute() or die(print_r(self::$data->errorInfo(), true));
    }

    /**
     * PDO Count Column SQL Query.
     *
     * @return object Count Column SQL Query.
     */
    public static function getCount()
    {
        return self::$data->columnCount();
    }

    /**
     * PDO Execute SQL Query.
     *
     * @return object Execute SQL Query.
     */
    public static function execPDO()
    {
        return parent::$connection->exec(self::$query);
    }

    /**
     * Fetch all data with array object.
     *
     * @return object Fetch Array Syntax for PDO.
     */
    public static function result()
    {
        return self::$data->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Fetch all data with array object.
     *
     * @return object Fetch Array Syntax for PDO.
     */
    public static function rowResult()
    {
        return self::$data->fetchAll(\PDO::FETCH_NUM);
    }

    /**
     * Fetch all data with json.
     *
     * @return object Fetch JSON for API.
     */
    public static function json()
    {
        return json_encode(self::$data->fetchAll(\PDO::FETCH_ASSOC));
    }

    public static function customJSON()
    {
        $data = self::$data->fetchAll(\PDO::FETCH_ASSOC);
        $output = [];
        foreach ($data as $value) {
            $output[] = array(
                "nama"   => $value['nama'],
                "kasir"  => date('H:i', strtotime($value['kasir'])),
                "kiwil"  => ($value['kiwil'] === null) ? "-" : date('H:i', strtotime($value['kiwil'])),
                "mesin"  => ($value['mesin'] === null) ? "-" : date('H:i', strtotime($value['mesin'])),
                "status" => $value['status']
            );
        }
        return json_encode($output);
    }

    public static function htmlJSON()
    {
        $data = self::$data->fetchAll(\PDO::FETCH_ASSOC);
        $output = [];
        foreach ($data as $value) {
            if ($value['status'] == "A") {
                $api = "'/api/checklist/kiwil/'";
            } elseif ($value['status'] == "B") {
                $api = "'/api/checklist/mesin/'";
            }
            $output[] = array(
                "nama"  => $value['nama'],
                "kasir" => date('H:i', strtotime($value['kasir'])),
                "kiwil" => date('H:i', strtotime($value['kiwil'])),
                "mesin" => date('H:i', strtotime($value['mesin'])),
                "aksi"  => '<input name="aksinya" onclick="aksinya(this, '.$value['id'].', '.$api.');" type="checkbox">'
            );
        }
        return json_encode($output);
    }
}
