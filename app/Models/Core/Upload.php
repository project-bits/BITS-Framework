<?php namespace BITS;

/**
 * BITS Upload Services.
 *
 * Class to simply upload file with additional name and make new folder if not exist.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @author Nurul Imam
 *
 * @link https://bits.co.id Banten IT Solutions
 *
 * @version 1.1
 */
class Upload
{
    public static function upload($file, $temp, $folder)
    {
        if (is_dir($folder) == false) {
            // Create folder if empty
            mkdir("$folder", 0755);
        }

        /*
         * Upload file if no exist.
         */
        if (is_dir("$folder/".$file) == false) {
            move_uploaded_file($temp, "$folder/".$file);
        }
    }

    public static function photo()
    {
        /*
         * Validate value NULL if file not exist.
         * If file exist, rename file to add date, lowercase and replace space to stripe.
         * Upload file to folder "public/files/img/".
         */
        if ($_FILES['photo']['name'] != '') {
            $name  = strtolower(str_replace(' ', '-', $_FILES['photo']['name']));
            $photo = '/files/img/'.strtolower(str_replace(' ', '-', $_FILES['photo']['name']));
            self::upload($name, $_FILES['photo']['tmp_name'], 'files/img/');
        } else {
            $photo = '';
        }

        return $photo;
    }

    public static function summernote()
    {
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $file = 'files/img/'.date('Y-m-d').'-'.strtolower(str_replace(' ', '-', $_FILES['file']['name']));
                $temp = $_FILES["file"]["tmp_name"];
                if (is_dir($file) == false) {
                    move_uploaded_file($temp, $file);
                }
                echo "/".$file;
            } else {
                echo 'Ooops!  Your upload triggered the following error : '.$_FILES['file']['error'];
            }
        }
    }
}
