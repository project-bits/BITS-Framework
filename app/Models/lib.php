<?php
use BITS\BITS;

/**
 * Library all functions use rendering frontend template.
 * Call directly in controller to use function and render in view using object defined in controller.
 *
 * Do not assign parameter in all function because controller not able call object with parameter.
 */

/**
 * SEO Library
 */

/**
 * Fetch Web Description From Database and return APPDESC is not exist.
 * @return string Web Description.
 */
function weburl()
{
    echo empty(APPURL) ? "/" : APPURL;
}

/**
 * Get Dynamic Title.
 * @return string Title of Post.
 */
function webtitle()
{
    if (is_null(APPTITLE)) {
        echo APPNAME." - ".APPDESC;
    } else {
        echo APPTITLE." - ".APPNAME;
    }
}
