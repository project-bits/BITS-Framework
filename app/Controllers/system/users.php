<?php
use App\User;
use BITS\Auth;

// View All
$this->respond('GET', '/?', function ($request, $response, $service) {
    $service->title = 'Users';
    $service->users = User::all();
    $service->render('app/Views/system/users.php');
});

// Create & Update
$this->respond('POST', '/?', function ($request, $response, $service) {
    if (isset($_POST['create'])) {
        User::add();
    } elseif (isset($_POST['update'])) {
        User::update();
    }
    Auth::redirect("/system/users/");
});

// Delete
$this->respond('GET', '/delete/[:id]', function ($request, $response, $service) {
    User::delete($request->id);
    Auth::redirect("/system/users/");
});
