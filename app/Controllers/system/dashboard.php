<?php
use BITS\BITS;

/**
 * GET Method Request
 */
$this->respond('GET', '/?', function ($request, $response, $service) {
    $service->title = 'Dashboard - '.BITS::appname();
    $service->render('app/Views/system/dashboard.php');
});
