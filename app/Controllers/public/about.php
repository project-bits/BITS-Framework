<?php
use BITS\BITS;

/**
 * GET Method Request
 */
$this->respond('GET', '/?', function ($request, $response, $service) {
    $service->title = 'About';
    define('APPTITLE', 'About');
    $service->render('themes/default/about.php');
});
