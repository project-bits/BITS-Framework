<?php
use BITS\Auth;
use BITS\BITS;
use BITS\Level;
use App\Proyek;
use App\Kriteria;
use App\Peserta;
use App\Pemenang;
use App\Penilaian;

/**
 * Uncomment if use database connections.
 * Please go to app/config/config.php & edit your database connection.
 */
new BITS;

/*
 * Default Page Controller.
 */
$route->respond('/', function ($request, $response, $service) {
    define('APPTITLE', 'Beranda');
    $service->layout('themes/default/index.php');
    $service->render('themes/default/home.php');
});

/*
 * Login Page Controller.
 */
$route->respond('/login/?', function ($request, $response, $service) {
    if (DBNAME == "") {
        $_SESSION['alert'] = 'danger';
        $_SESSION['message'] = 'Please Configure & Install Database...!';
    }
    $service->title = BITS::appname()." - System Login";
    $service->render('app/Views/system/login.php');
});

/*
 * Check and Validate user login.
 * If user successfully logged in, redirect to dashboard.
 */
$route->respond('POST', '/login/?', function ($request, $response, $service) {
    if (isset($_POST['submit'])) {
        Auth::login("users", $_POST['username'], $_POST['password']);
        if (isset($_SESSION['salt']) && isset($_SESSION['username'])) {
            Auth::redirect('/system/dashboard/');
        } else {
            Auth::redirect('/login/');
        }
    }
});

/*
 * Logout Page Controller.
 * Destroy all session and redirect to login page.
 */
$route->respond('/logout/?', function ($request, $response, $service) {
    Auth::logout();
    Auth::redirect('/');
});

/*
 * Initialize Frontend layouts layout.
 */
$route->respond(function ($request, $response, $service, $app) use ($route) {

    /*
     * Handle Error Exception message to all Controllers.
     */
    $route->onError(function ($route, $err_msg) {
        $route->service()->flash($err_msg);
        $route->service()->back();
    });

    /*
     * Register layouts master layout to all Controllers.
     */
    $service->layout('themes/default/index.php');
});

/**
 * Frontend Controllers.
 */
foreach (['about'] as $controller) {
    $route->with("/$controller", "app/Controllers/public/$controller.php");
}


/*
 * Initialize Controllers with some service and layouts layout.
 * Add controller to this block or separate use loop Controllers.
 */
$route->respond(function ($request, $response, $service, $app) use ($route) {

    /*
     * Handle Error Exception message to all Controllers.
     */
    $route->onError(function ($route, $err_msg) {
        $route->service()->flash($err_msg);
        $route->service()->back();
    });

    /*
     * Register layouts master layout to all Controllers.
     */
    $service->layout('app/Views/system/layouts/index.php');
});

/**
 * System Administrator Controllers.
 */
foreach (['dashboard', 'users'] as $controller) {
    /**
     * Check is admin or users. Protect page only administrator to
     * access this page / controller.
     */
    if (Level::isAdmin()) {
        $route->with("/system/$controller", "app/Controllers/system/$controller.php");
    }
}

/*
 * Route all error request to error page.
 */
$route->onHttpError(function ($code, $router, $service) {
    if ($code >= 400 && $code < 500) {
        include 'themes/default/404.php';
    } elseif ($code >= 500 && $code <= 599) {
        include 'themes/default/404.php';
    }
});
