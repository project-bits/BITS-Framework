<div class="modal inmodal fullscreen update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <form autocomplete="off" role="form" data-toggle="validator" method="POST" action="" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-users modal-icon"></i>
                    <h5 class="modal-title">Edit User</h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username</label>
                            <input name="id" type="hidden" id="id" />
                            <input name="username" id="username" type="text"
                                    class="form-control"
                                    placeholder="Username"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input name="email" id="email2" type="email"
                                    class="form-control"
                                    placeholder="Email"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label>Password</label>
                            <input name="password" id="password" type="password"
                                class="form-control"
                                placeholder="Leave blank if do not to change"
                                data-minlength="6"
                                data-error="Please fill min 6 word." />
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Full Name</label>
                            <input name="name" id="name" type="text"
                                    class="form-control"
                                    placeholder="Full Name"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label>No. Telp</label>
                            <input name="telp" id="telp" type="text"
                                    class="form-control"
                                    placeholder="No. Telp"
                                    maxlength="15" required />
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label>Address</label>
                            <input name="address" id="address"
                                    class="form-control"
                                    placeholder="Full Address"
                                    maxlength="190" type="text" />
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Photo</label>
                            <input id="photo2" class="form-control" type="file" accept="image/*" name="photo" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm btn-submit" name="update">
                        <i class="fa fa-save"></i> Save
                    </button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-close"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
