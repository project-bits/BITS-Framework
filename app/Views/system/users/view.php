<div class="modal inmodal view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <form autocomplete="off" role="form" data-toggle="validator" method="POST" action="" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <img class="img-circle" id="vphoto" src="" alt="Photo" width="64" height="64" />
                    <h5 class="modal-title" id="vname"></h5>
                </div>
                <div class="modal-body">
                    <table class="table table-stripe">
                        <tbody>
                            <tr>
                                <td>Username</td>
                                <td id="vusername"></td>
                            </tr>
                            <tr>
                                <td>Email Address</td>
                                <td id="vemail"></td>
                            </tr>
                            <tr>
                                <td>No. Telp</td>
                                <td id="vtelp"></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td id="valamat"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-close"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
