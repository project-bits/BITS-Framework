<?php
use BITS\Auth;
use BITS\BITS;

?>
<div class="row dashboard-header">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 class="text-success">Total Users</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php //echo $this->totalusers; ?>100</h1>
                    <small>Registered Users</small>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 class="text-success">Total Login</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php //echo $this->totallogin; ?>100</h1>
                    <small>Login</small>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 class="text-success">Total Failure</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php //echo $this->totalfailure; ?>100</h1>
                    <small>Failure Login</small>
                </div>
            </div>
        </div>
    </div>
</div>
