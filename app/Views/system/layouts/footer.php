            <div class="clearfix"></div>
            <div class="top10">&nbsp;</div>
            <div class="top10">&nbsp;</div>
            <div class="top10">&nbsp;</div>
            <div class="clearfix"></div>

            <div class="footer">
                <div class="pull-right">
                    <i class="fa fa-paper-plane-o"></i> &nbsp; BITS Framework.
                </div>
                <div>
                    &copy; Copyright 2014 - <?php echo date('Y'); ?> <?php echo APPNAME; ?>. All Right Reserved.
                </div>
            </div>
        </div>
    </div>

    <script src="/assets/bits-frontend/js/script.js"></script>
    <script>
<?php
use Tools\Alert;

Alert::view(); ?>
    </script>
</body>
</html>
