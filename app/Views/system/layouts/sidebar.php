<?php
use App\User;

?>
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span>
                                <img alt="image" class="img-circle" width="44" height="44" src="<?php User::getPhoto(); ?>">
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php User::getName(); ?></strong>
                                 </span> <span class="text-muted text-xs block"><?php User::getMail(); ?> <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="#">My Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="/logout/">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            <i class="fa fa-diamond"></i>
                        </div>
                    </li>
                    <li>
                        <a href="/system/dashboard/"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li>
                        <a href="/system/users/"><i class="fa fa-user"></i> <span class="nav-label">Users</span></a>
                    </li>
                    <li>
                        <a href="/logout/"><i class="fa fa-undo"></i> <span class="nav-label">Logout</span></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" class="white-bg">
<?php $this->partial('app/Views/system/layouts/menu.php'); ?>
