            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="/" target="_blank">
                                <i class="fa fa-link text-info"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/logout/">
                                <i class="fa fa-sign-out text-info"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
