<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->title; ?></title>
    <link href="/assets/bits-frontend/css/style.min.css" rel="stylesheet">
    <style type="text/css">
        @import url(http://fonts.googleapis.com/css?family=Pacifico:100,300,400,700);
        @import url(http://fonts.googleapis.com/css?family=Dancing+Script:100,300,400,700);
    </style>
</head>
<body>
<?php
use Tools\Alert;
use BITS\BITS;

?>
    <div class="container">
        <div class="row" style="margin-top:90px">
            <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
                <!--img class="center-block img-responsive" src="/assets/bits-frontend/img/backgrounds/bits.jpg" alt="logo"-->
                <form class="loginnya" role="form" data-toggle="validator" action="" method="post" class="form">
                <h1 class="text-center text-success not"><?php echo BITS::appname($this->name); ?></h1>
                <h3 class="text-center text-success change"><?php echo BITS::appdesc($this->desc); ?></h3>
                    <fieldset>
                        <?php Alert::show(); ?>
                        <hr class="colorgraph">
                        <div class="form-group">
                            <input type="text" name="username" placeholder="Username..." class="form-control" id="username" data-error="Please fill your username." required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Password..." class="form-control" id="password" data-error="Please fill your password." required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 pull-right">
                                <button type="submit" class="btn btn-lg btn-success btn-block" name="submit">Sign In</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <script src="/assets/bits-frontend/js/script.js"></script>
</body>
</html>
